var express = require( 'express' );
var cons    = require( 'consolidate' );
var app     = express();

app.engine( 'html' , cons.swig );
app.set( 'view engine' , 'html' );
app.set( 'views' , __dirname + '/views' );
//app.use( app.router );

// handler for internal storage errors 
function errorHandler( err , req , res , next ) {
	console.error( err.message );
	console.error( err.stack );
	res.status( 500 );
	res.render( 'error_template' , { error : err } );
}

app.use( errorHandler );

app.get( '/:name' , function( req , res , next ){
	var name    = req.params.name;
	var getvar1 = req.query.getvar1;
	var getvar2 = req.query.getvar2;
	res.render( 'get' , { name : name , getvar1 : getvar1 , getvar2 : getvar2 } );
} );

app.listen( 3000 );
console.log( 'Express server listening on port 3000' );
console.log( 'use URL: localhost:3000/javier?getvar1=blah\?getvar2=hi' );
console.log( 'something like that' );
